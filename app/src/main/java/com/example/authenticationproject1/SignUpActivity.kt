package com.example.authenticationproject1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*


@Suppress("UNREACHABLE_CODE")
class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener{
            signUp()
            checkEmail()
    }
    }

    private fun signUp(){
        val email=emailEditText.text.toString()
        val password=passwordEditText.text.toString()
        val repeatPassword=repeatPasswordEditText.text.toString()


        if(email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty() && emailEditText.text.toString().isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches() ){
            if(password == repeatPassword ){
                progressBar.visibility = View.VISIBLE
                signUpButton.isClickable=false
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            signUpButton.isClickable=true
                            progressBar.visibility =View.GONE
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("signUp", "createUserWithEmail:success")
                                auth.currentUser
                                Toast.makeText(this, "signUp is success", Toast.LENGTH_SHORT).show()

                            } else {
                                // If sign in fails, display a message to the user.
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show()

                            }

                        }
            }else{
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }


    }

    fun checkEmail() =
            if(emailEditText.text.toString().isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches()) {
                Toast.makeText(this, "Email validated successfully", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Email format is not correct", Toast.LENGTH_SHORT).show()
            }
}